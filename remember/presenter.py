from PyQt6.QtWidgets import QWidget

class Presenter(QWidget):
    def __init__(self):
        QWidget.__init__(self)

    def setHtml(self, html: str):
        pass

    def setZoom(self, zoom: float):
        pass

    def findText(self, text: str, backward: bool):
        pass

    def widget(self) -> QWidget:
        pass

    def isVisible(self) -> bool:
        pass

