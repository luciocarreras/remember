class History():
    def __init__(self, dataModel):
        self.history = []
        self.currentIndex = -1

        dataModel.noteRenamed.connect(self.itemRenamed)
        dataModel.noteRemoved.connect(self.itemRemoved)

    def backward(self):
        if self.currentIndex > 0:
            self.currentIndex = self.currentIndex - 1
            return self.history[self.currentIndex]
        return None

    def forward(self):
        if self.currentIndex < len(self.history) - 1:
            self.currentIndex = self.currentIndex + 1
            return self.history[self.currentIndex]
        return None

    def nextItem(self):
        return self.history[self.currentIndex + 1] if self.currentIndex < len(self.history) - 1 else None

    def previousItem(self):
        return self.history[self.currentIndex - 1] if self.currentIndex > 0 else None

    def changeItem(self, item):
        if len(self.history) > 0:
            currentItem = self.history[self.currentIndex]
            if currentItem == item:
                return

        if n := self.nextItem():
            if not n == item:
                self.history = self.history[:self.currentIndex + 1]
                self.history.append(item)
        else:
            self.history.append(item)

        self.forward()

    def itemRenamed(self, oldName, newName):
        self.history = [newName if x == oldName else x for x in self.history]

    def itemRemoved(self, item):
        while item in self.history:
            index = self.history.index(item)
            if index <= self.currentIndex:
                self.currentIndex = self.currentIndex - 1
            self.history.remove(item)
