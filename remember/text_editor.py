import common

from PyQt6.QtCore import Qt, pyqtSignal
from PyQt6.QtGui import QTextDocument
from PyQt6.QtWidgets import QPlainTextEdit

import math
import os


def insertImagePath(editor, imagePath):
    text = f'![]({imagePath})'
    editor.textCursor().insertText(text)


def hasWhitespacesBefore(editor, whitespaces):
    text = editor.toPlainText()
    pos = editor.textCursor().position()
    if whitespaces > 0:
        if pos >= whitespaces:
            if all(c == ' ' for c in text[pos - whitespaces:pos]):
                return whitespaces
        else:
            return 0


def insertSpaces(editor, whitespaces):
    for i in range(0, whitespaces):
        editor.textCursor().insertText(' ')


def removeCharsBefore(editor, count):
    for i in range(0, count):
        editor.textCursor().deletePreviousChar()


class MarkdownTextEditor(QPlainTextEdit):
    zoomFactorChanged = pyqtSignal(float)
    saveTriggered = pyqtSignal()

    def __init__(self, settings, parent):
        QPlainTextEdit.__init__(self, parent)
        self.settings = settings
        self.setStyleSheet(common.getMarkdownStyleSheet())
        self.setFont(common.getMarkdownFont())

    def keyPressEvent(self, event):
        tabStop = 4
        if event.modifiers() & Qt.KeyboardModifier.ControlModifier:
            if event.key() == Qt.Key.Key_S:
                self.saveTriggered.emit()
            elif event.key() == Qt.Key.Key_Plus:
                self.changeZoom(self.settings.loadZoomFactor() * 1.1)
            elif event.key() == Qt.Key.Key_Minus:
                self.changeZoom(self.settings.loadZoomFactor() / 1.1)
            else:
                QPlainTextEdit.keyPressEvent(self, event)
        elif event.key() == Qt.Key.Key_Escape:
            self.saveTriggered.emit()
        elif event.key() == Qt.Key.Key_Tab:
            insertSpaces(self, tabStop)
        elif event.key() == Qt.Key.Key_Backtab:
            removeCharsBefore(self, tabStop)
        elif event.key() == Qt.Key.Key_Backspace:
            if hasWhitespacesBefore(self, tabStop):
                removeCharsBefore(self, tabStop)
            else:
                QPlainTextEdit.keyPressEvent(self, event)
        else:
            QPlainTextEdit.keyPressEvent(self, event)

    def canInsertFromMimeData(self, mimeData):
        if mimeData.hasImage():
            return True
        return QPlainTextEdit.canInsertFromMimeData(self, mimeData)

    def insertFromMimeData(self, mimeData):
        if mimeData.hasImage():
            targetDirectory = os.path.join(self.settings.loadDirectory(), 'images')
            imagePath = common.saveClipboardImage(targetDirectory, mimeData.imageData())
            insertImagePath(self, imagePath)
        else:
            QPlainTextEdit.insertFromMimeData(self, mimeData)

    def jumpToEnd(self):
        text = self.toPlainText()
        cursor = self.textCursor()
        cursor.setPosition(len(text))
        self.setTextCursor(cursor)

    def findText(self, text, backward):
        def isCursorValid(cursor):
            return cursor and not cursor.isNull()

        doc = self.document()

        plainTextFlags = QTextDocument.FindFlag.FindBackward if backward else QTextDocument.FindFlag(0)
        startOverIndex = len(doc.toPlainText()) - 1 if backward else 0

        c = doc.find(text, self.textCursor(), plainTextFlags)
        if not isCursorValid(c):
            c = doc.find(text, startOverIndex, plainTextFlags)
        if isCursorValid(c):
            self.setTextCursor(c)

    def changeZoom(self, zoomFactor: float):
        self.setZoomFactor(zoomFactor)
        self.zoomFactorChanged.emit(zoomFactor)

    def setZoomFactor(self, zoomFactor: float):
        font = common.getMarkdownFont()
        font.setPointSize(math.floor(font.pointSize() * zoomFactor))
        self.setFont(font)
