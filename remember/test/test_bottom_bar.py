from bottom_bar import BottomBar

from PyQt6.QtCore import Qt

import os
import pytest
import shutil

def addDirectory(path, settings):
    os.mkdir(path)
    notes = [
        os.path.join(path, 'note1.md'), 
        os.path.join(path, 'note2.md'), 
        os.path.join(path, 'note3.md')]

    for note in notes:
        with open(note, 'w') as f:
            f.write('')

    lastDirectories = settings.loadLastDirectories()
    if not lastDirectories:
        lastDirectories = []

    lastDirectories.append(path)
    settings.saveLastDirectories(lastDirectories)

    return notes


@pytest.fixture
def directories(notes_directory, settings):
    dirs = ['dir1', 'dir2', 'dir3']
    paths = []
    for d in dirs:
        path = os.path.join(notes_directory, d)
        paths.append(path)
        addDirectory(path, settings)

    settings.saveDirectory(path[0])
    settings.saveLastDirectories(paths)

    assert len(paths) == 3

    yield paths

    for path in paths:
        shutil.rmtree(path)


def test_fill_combo_box(directories, settings, qtbot):
    index = 1
    numEntries = len(directories)
    settings.saveDirectory(directories[index])
    bottomBar = BottomBar(settings)
    assert bottomBar.comboDirectories.currentIndex() == index
    assert bottomBar.comboDirectories.count() == numEntries
    for i in range(0, 3):
        assert bottomBar.comboDirectories.itemText(i) == directories[i]
    

def test_add_dir(notes_directory, directories, settings, qtbot):
    bottomBar = BottomBar(settings)
    numEntries = len(directories)
    path = os.path.join(notes_directory, 'bla')

    def addDirClicked():
        addDirectory(path, settings)

    bottomBar.addDirClicked.connect(addDirClicked)
    qtbot.mouseClick(bottomBar.btnAddDir, Qt.MouseButton.LeftButton)

    assert bottomBar.comboDirectories.count() == numEntries + 1
    assert bottomBar.comboDirectories.itemText(numEntries) == path

def test_remove_dir(directories, settings, qtbot):
    bottomBar = BottomBar(settings)

    def deleteDirClicked(path):
        savedDirectories = settings.loadLastDirectories()
        savedDirectories.remove(path)
        settings.saveLastDirectories(savedDirectories)

    bottomBar.deleteDirClicked.connect(deleteDirClicked)
    qtbot.mouseClick(bottomBar.btnDeleteDir, Qt.MouseButton.LeftButton)

    assert bottomBar.comboDirectories.count() == 2


def test_current_index_changed(directories, settings, qtbot):
    bottomBar = BottomBar(settings)
    def dirSelected(dirName):
        global callback_called
        try:
            callback_called = callback_called + 1
        except: 
            callback_called = 1

    # hack to somehow trigger bottom.selectedDirChanged because
    # it's only intended to be triggered with activated(index)
    bottomBar.comboDirectories.currentIndexChanged.connect(bottomBar.selectedDirChanged)
    bottomBar.dirSelected.connect(dirSelected)
    bottomBar.comboDirectories.setCurrentIndex(2)
    bottomBar.comboDirectories.setCurrentIndex(1)
    bottomBar.comboDirectories.setCurrentIndex(0)

    assert callback_called == 3
