FROM ubuntu:focal

COPY pypi/requirements.txt /tmp/requirements.txt

ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt -yq upgrade && apt -yq install python3-pip make
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install -r /tmp/requirements.txt

RUN apt update && apt install -yq libasound2 libdbus-1-3 libegl1 libfontconfig1 libgl1-mesa-glx libglib2.0-0 libgssapi-krb5-2 libnss3 libxcomposite1 libxdamage1 libxkbcommon0 libxkbfile1 libxi6 libxrandr2 libxrender1 libxtst6 

