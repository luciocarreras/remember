import common

from PyQt6.QtWidgets import (QComboBox, QWidget, QPushButton, QSizePolicy, QHBoxLayout)
from PyQt6.QtCore import pyqtSignal


def initDirectories(comboBox, settings):
    comboBox.clear()
    directories = settings.loadLastDirectories()
    lastDirectory = settings.loadDirectory()
    if not directories and lastDirectory:
        directories = [lastDirectory]
        settings.saveLastDirectories(directories)

    if directories:
        for directory in directories:
            comboBox.addItem(directory, directory)

    if lastDirectory:
        comboBox.setCurrentText(lastDirectory)


class BottomBar(QWidget):
    dirSelected = pyqtSignal(str)
    addDirClicked = pyqtSignal()
    deleteDirClicked = pyqtSignal(str)

    def __init__(self, settings, parent=None):
        QWidget.__init__(self, parent)

        self.settings = settings

        layout = QHBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)

        self.comboDirectories = QComboBox(self)
        initDirectories(self.comboDirectories, settings)
        self.btnAddDir = QPushButton(self)
        self.btnAddDir.setIcon(common.getAddIcon())
        self.btnDeleteDir = QPushButton(self)
        self.btnDeleteDir.setIcon(common.getRemoveIcon())

        layout.addWidget(self.comboDirectories)
        layout.addWidget(self.btnAddDir)
        layout.addWidget(self.btnDeleteDir)
        self.comboDirectories.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Preferred)

        self.comboDirectories.activated.connect(self.selectedDirChanged)
        self.btnAddDir.clicked.connect(self.addDirTriggered)
        self.btnDeleteDir.clicked.connect(self.deleteDirTriggered)

        self.setLayout(layout)

    def addDirTriggered(self, b):
        self.addDirClicked.emit()
        initDirectories(self.comboDirectories, self.settings)

    def deleteDirTriggered(self, b):
        self.deleteDirClicked.emit(self.comboDirectories.currentData())
        initDirectories(self.comboDirectories, self.settings)

    def selectedDirChanged(self, index):
        directory = self.comboDirectories.currentData()
        self.dirSelected.emit(directory)

    def setCurrentDir(self, dir):
        self.comboDirectories.blockSignals(True)
        self.comboDirectories.setCurrentText(dir)
        self.comboDirectories.blockSignals(False)
