import common

from presenter import Presenter

from PyQt6.QtWebEngineWidgets import QWebEngineView
from PyQt6.QtWebEngineCore import QWebEnginePage
from PyQt6.QtWidgets import QMenu
from PyQt6.QtCore import QUrl
from PyQt6.QtGui import QDesktopServices


class WebEnginePage(QWebEnginePage):
    def __init__(self, noteWidget):
        QWebEnginePage.__init__(self, noteWidget)
        self.noteWidget = noteWidget

    def acceptNavigationRequest(self, url, _type, isMainFrame):
        if _type == QWebEnginePage.NavigationType.NavigationTypeLinkClicked:
            if url.scheme() == 'remember':
                self.noteWidget.onLinkClicked(url.fileName())
            else:
                QDesktopServices.openUrl(url)
            return False

        return True


class WebEnginePresenter(Presenter):
    class WebEngineView(QWebEngineView):
        def __init__(self, settings, parent=None):
            QWebEngineView.__init__(self, parent)
            self.settings = settings
            self.setPage(WebEnginePage(parent))
            self.setZoomFactor(self.settings.loadZoomFactor())

        def contextMenuEvent(self, event):
            action = self.pageAction(QWebEnginePage.WebAction.CopyLinkToClipboard)
            if action:
                menu = QMenu(self)
                menu.addAction(action)
                menu.exec(event.globalPos())

    def __init__(self, settings, parent):
        Presenter.__init__(self)
        self.webEngineView = WebEnginePresenter.WebEngineView(settings, parent)

    def setHtml(self, html):
        doc = '''<head>
            <style>{css}</style>
        </head>
        <body>
        {body}
        </body>'''.format(css=common.readCSSFile(), body=html)
        self.webEngineView.setHtml(doc, QUrl('file:///'))

    def findText(self, text, backward):
        flag = QWebEnginePage.FindFlag.FindBackward if backward else QWebEnginePage.FindFlag(0)
        self.webEngineView.findText(text, flag)

    def setZoom(self, zoom: float):
        self.webEngineView.setZoomFactor(zoom)

    def widget(self):
        return self.webEngineView

    def isVisible(self):
        return self.webEngineView.isVisible()
