from PyQt6.QtCore import Qt, QTimer
from PyQt6.QtWidgets import QApplication, QMessageBox

import os
import pytest


def find_file(notes_directory, name):
    files = os.listdir(notes_directory)
    return files.count(name)


def find_in_list_widget(list_widget, name):
    items = list_widget.findItems(name, Qt.MatchFlag.MatchExactly)
    return len(items)


def delete_current_item(window, qtbot):
    def close_modal_dialog():
        QApplication.processEvents()
        button = window.deleteDialog.button(QMessageBox.StandardButton.Yes)
        qtbot.mouseClick(button, Qt.MouseButton.LeftButton)

    QTimer.singleShot(100, close_modal_dialog)
    window.deleteClicked()


def test_window_title(window, notes_directory, qtbot):
    assert window.windowTitle() == str(notes_directory)


@pytest.mark.parametrize('note_count', [0], indirect=True)
def test_populate_list_widget_without_notes(window, data_model, qtbot):
    assert data_model.count() == 0
    assert window.listWidget.count() == data_model.count()
    assert window.searchBar.btnDelete.isEnabled() is False


def test_populate_list_widget(notes, data_model, window, qtbot):
    assert data_model.count() == len(notes)
    assert window.listWidget.count() == data_model.count()
    assert window.searchBar.btnDelete.isEnabled()


def test_populate_keeps_current_index(window, qtbot):
    assert window.listWidget.count() > 5
    window.listWidget.setCurrentRow(4)
    window.populateListWidget()
    assert window.listWidget.currentRow() == 4


@pytest.mark.parametrize('note_count', [0], indirect=True)
def test_new(notes_directory, window, data_model, qtbot):
    window.newClicked(False)

    assert data_model.count() == 1
    assert data_model.files()[0] == "New.md"
    assert find_file(notes_directory, "New.md") == 1

    for i in range(1, 5, 1):
        window.newClicked(False)
        expected_filename = f'New{i}.md'

        assert data_model.count() == i + 1
        assert window.listWidget.count() == data_model.count()
        assert data_model.files()[i] == expected_filename
        assert find_file(notes_directory, expected_filename) == 1
        assert window.listWidget.currentItem().text() == expected_filename


def test_delete(window, notes_directory, data_model, qtbot):
    item_count = data_model.count()
    assert data_model.count() > 5

    window.listWidget.setCurrentRow(4)

    while window.listWidget.count() > 0:
        expected_filename = window.listWidget.currentItem().text()
        assert find_file(notes_directory, expected_filename) == 1

        delete_current_item(window, qtbot)

        assert data_model.count() == item_count - 1
        assert window.listWidget.count() == data_model.count()
        assert find_file(notes_directory, expected_filename) == 0
        assert window.listWidget.currentRow() == min(4, window.listWidget.count() - 1)

        item_count = item_count - 1

    assert window.searchBar.btnDelete.isEnabled() is False


@pytest.mark.skipif(len(os.environ['DISPLAY']) == 0, reason='https://github.com/pytest-dev/pytest-qt/issues/206')
def test_delete_focus(window, data_model, qtbot):
    window.listWidget.setCurrentRow(0)

    window.noteWidget.setMarkdown(data_model.files()[0], data_model.baseDir())
    window.noteWidget.showMarkdown()
    qtbot.wait_until(lambda: window.noteWidget.markdownEditor.hasFocus(), timeout=1000)

    delete_current_item(window, qtbot)

    # implicitly done by window.listWidget.showHtml()
    # Cannot be tested without WM because the window does not get the focus back
    # after showing the delete dialog
    qtbot.wait_until(lambda: window.listWidget.hasFocus(), timeout=1000)
    assert window.listWidget.hasFocus()


def test_rename(window, data_model, notes_directory, qtbot):
    items_count = data_model.count()

    for i in range(0, items_count, 1):
        window.listWidget.setCurrentRow(i)
        old_name = window.listWidget.currentItem().text()
        new_name = 'z_' + old_name

        assert data_model.files().count(old_name) == 1
        assert data_model.files().count(new_name) == 0
        assert find_in_list_widget(window.listWidget, old_name) == 1
        assert find_in_list_widget(window.listWidget, new_name) == 0

        filepath = notes_directory / old_name
        filepath.rename(notes_directory / new_name)
        data_model.rename(old_name, new_name)

        assert data_model.count() == items_count
        assert window.listWidget.count() == items_count
        assert data_model.files().count(old_name) == 0
        assert data_model.files().count(new_name) == 1
        assert find_in_list_widget(window.listWidget, old_name) == 0
        assert find_in_list_widget(window.listWidget, new_name) == 1
        assert window.listWidget.currentItem().text() == new_name


def test_filter(window, data_model, qtbot):
    window.searchTextEdited('nice')

    assert data_model.count() == 5
    assert window.listWidget.count() == data_model.count()

    for i in range(0, 5, 1):
        expected_text = 'note {}.md'.format(i * 2)
        assert window.listWidget.item(i).text() == expected_text

    window.searchTextEdited('4')
    assert data_model.count() == 1
    assert window.listWidget.count() == data_model.count()

    window.searchTextEdited('ugly')
    assert data_model.count() == 4
    assert window.listWidget.count() == data_model.count()


def test_clear_filter(window, data_model, qtbot):
    original_count = data_model.count()

    window.searchTextEdited('nice')
    assert data_model.count() < original_count
    assert window.listWidget.count() == data_model.count()

    window.searchTextEdited('')
    assert data_model.count() == original_count
    assert window.listWidget.count() == data_model.count()


def test_filter_new(window, data_model, qtbot):
    original_count = data_model.count()

    window.searchTextEdited('nice')
    item_count = data_model.count()
    assert item_count < original_count

    window.newClicked()

    assert data_model.count() == item_count + 1
    assert window.listWidget.count() == data_model.count()


def test_filter_delete(window, data_model, qtbot):
    original_count = data_model.count()

    window.searchTextEdited('nice')
    item_count = data_model.count()
    assert item_count < original_count

    window.listWidget.setCurrentRow(3)
    delete_current_item(window, qtbot)

    assert data_model.count() == item_count - 1
    assert window.listWidget.count() == data_model.count()


def test_filter_new_and_delete(window, data_model, qtbot):
    original_count = data_model.count()

    window.searchTextEdited('nice')
    item_count = data_model.count()
    assert item_count < original_count

    window.newClicked()
    window.listWidget.setCurrentRow(3)
    delete_current_item(window, qtbot)

    assert data_model.count() == item_count
    assert window.listWidget.count() == item_count


@pytest.mark.parametrize('note_count', [0], indirect=True)
def test_last_note_if_no_notes_available(window, qtbot):
    assert window.listWidget.currentRow() == -1


def test_last_note_if_not_available(window, qtbot):
    assert window.listWidget.currentRow() == 0


@pytest.mark.parametrize('settings_content', [{'last_note': 'note 4.md'}], indirect=True)
def test_last_note_if_available(window, qtbot):
    assert window.listWidget.currentItem().text() == 'note 4.md'


@pytest.mark.parametrize('settings_content', [{'last_note': 'not-available.md'}], indirect=True)
def test_last_note_if_available(window, qtbot):
    assert window.listWidget.currentRow() == 0
