from datetime import datetime
import markdown
from markdown.extensions.toc import TocExtension
import os
import re

from PyQt6.QtGui import (QFont, QIcon)
from PyQt6.QtWidgets import (QFileIconProvider, QFileDialog)


def isValidMarkdownFile(filename):
    return os.path.isfile(filename) and filename.endswith('.md')


def getFiles(directory):
    if not directory:
        return []

    lst = [f for f in os.listdir(directory)
           if isValidMarkdownFile(os.path.join(directory, f))]
    return lst


def getHiliteConfig():
    return {
        'codehilite': {
            'linenums': False
        }
    }


def getMarkdownStyleSheet():
    return "background-color: #fff4db; color: black;"


def getMarkdownFont():
    return QFont("Courier New", -1)


def escapeSpecialChars(title):
    for c in ['/', '*']:
        title = title.replace(c, '-')
    return title


def cleanTitle(title):
    title = title.strip()
    if len(title) == 0:
        title = 'New'
    return title


def getFilenameFromTitle(title):
    title = cleanTitle(title)

    return "{}.md".format(escapeSpecialChars(title))


def getNewDocumentTitle(baseDir, basePrefix, overwrite=False):
    title = cleanTitle(basePrefix)
    filePath = os.path.join(baseDir, getFilenameFromTitle(title))
    i = 1
    while os.path.exists(filePath) and not overwrite:
        title = "{}{}".format(cleanTitle(basePrefix), i)
        filePath = os.path.join(baseDir, getFilenameFromTitle(title))
        i = i + 1

    return title, filePath


def deleteNote(directory, filename):
    path = os.path.join(directory, filename)
    try:
        os.remove(path)
    except:
        pass


def saveNote(directory, filename, data):
    filepath = os.path.join(directory, filename)
    try:
        with open(filepath, 'w') as f:
            f.write(data)
        return True
    except:
        return False


def replaceTextInAllNotes(directory, oldText, newText):
    allNotes = getFiles(directory)
    for note in allNotes:
        fullPath = os.path.join(directory, note)
        replaced = False
        with open(fullPath, 'r') as f:
            data = f.read()
            if oldText in data:
                data = data.replace(oldText, newText)
                replaced = True

        if replaced:
            with open(fullPath, 'w') as f:
                f.write(data)


def renameNote(directory, filename, data):
    prefix = getTitleFromContent(data)
    title, filepath = getNewDocumentTitle(directory, prefix, False)
    newFilename = getFilenameFromTitle(title)
    if saveNote(directory, newFilename, data):
        deleteNote(directory, filename)
        replaceTextInAllNotes(directory, filename, newFilename)
        return newFilename

    return None


def filterTitles(directory, filenames, searchTerm):
    if not searchTerm:
        return filenames

    matchingFiles = []
    for filename in filenames:
        filepath = os.path.join(directory, filename)
        with open(filepath) as f:
            data = f.read()
            if searchTerm.lower() in data.lower():
                matchingFiles.append(filename)

    return matchingFiles


def getTitleFromContent(data):
    data = data.partition('\n')[0]
    data = re.sub(r'[`#\*\~\(\)!\?]', '', data)
    return data.strip()


def getEditIcon():
    return QIcon.fromTheme("accessories-text-editor")


def getDirectoryIcon():
    return QFileIconProvider().icon(QFileIconProvider.IconType.Folder)


def getAddIcon():
    return QIcon.fromTheme("list-add")


def getRemoveIcon():
    return QIcon.fromTheme("list-remove")


def saveClipboardImage(basePath, image):
    if not os.path.exists(basePath):
        os.mkdir(basePath)

    date = datetime.now()
    datestr = date.strftime("%Y%b%d-%H%M%S")
    path = os.path.join(basePath, 'clipboard-{}.png'.format(datestr))
    image.save(path)
    return path


def handleImages(html):
    return re.sub(r'<img(.*?)src="(.*?)"(.*?)>', r'<a href="\2"><img\1src="\2"\3height="20%"></a>', html)


def convertMarkdownToHtml(data):
    return '' if not data else markdown.markdown(
        data,
        extensions=['markdown.extensions.fenced_code',
                    'markdown.extensions.codehilite',
                    'markdown.extensions.tables',
                    TocExtension(baselevel=3)],
        extension_configs=getHiliteConfig())


def readCSSFile():
    currentDir = os.path.dirname(os.path.abspath(__file__))
    filepath = os.path.join(currentDir, 'data', 'remember.css')
    with open(filepath) as f:
        css = f.read()
        return css


def createNewNote(directory):
    title, filepath = getNewDocumentTitle(directory, "New")
    with open(filepath, 'w') as f:
        f.write("# {}".format(title))

    return title


def chooseNewDir(parent):
    return QFileDialog.getExistingDirectory(
        parent,
        "Choose directory",
        os.path.expanduser("~"),
        QFileDialog.Option.ShowDirsOnly)
