#!/bin/bash

set -e

DISTDIR="pyinstaller/dist"
BUILDDIR="pyinstaller/build"
TARGETPREFIX="/opt"
PACKAGENAME="remember"

rm -rf ${DISTDIR} ${BUILDDIR}
pyinstaller \
	--name ${PACKAGENAME} \
	--windowed \
	--add-data remember/data/remember.css:data \
	--add-data resources/qt.conf:. \
	--add-data resources/remember.desktop:. \
	--onedir \
	--distpath ${DISTDIR} \
	--workpath ${BUILDDIR} \
	--exclude-module tkinter \
	remember/remember.py

unneeded="libQt5Bluetooth libQt5Location libQt5Multimedia libasound libavcodec libavformat libx265 libx264 libsqlite3 libgnutls"

for l in ${unneeded} ; do 
    rm -f ${DISTDIR}/remember/${l}*
done

# rm -rf ${DISTDIR}/remember/PyQt5/Qt/translations/*

LIB_EXEC_DIR="${DISTDIR}/remember/PyQt5/Qt/libexec"
mkdir -p ${LIB_EXEC_DIR}
find ${DISTDIR}/remember -name "QtWebEngineProcess" -exec cp {} ${LIB_EXEC_DIR}/ \;

DST="${TARGETPREFIX}/${PACKAGENAME}"
rm -rf ${DST}
mkdir -p ${DST}
cp -rv ${DISTDIR}/${PACKAGENAME}/* ${DST}/
