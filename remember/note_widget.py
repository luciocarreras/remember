import common
from text_editor import MarkdownTextEditor
from web_presenter import WebEnginePresenter
# from document_presenter import DocumentPresenter

from PyQt6.QtCore import QUrl, pyqtSignal
from PyQt6.QtGui import QKeySequence, QShortcut
from PyQt6.QtWidgets import QHBoxLayout, QVBoxLayout, QLineEdit, QPushButton, QStackedWidget, QWidget

import re
import os


def createReference(filename):
    encoded = QUrl.toPercentEncoding(f'{filename}')
    return '[{}](remember:///{})'.format(filename, str(encoded, 'utf-8').lower())


def referenceNotes(data, directory):
    files = common.getFiles(directory)
    for filename in files:
        r = createReference(filename)
        index = data.lower().find(filename.lower())
        if index >= 0:
            data = data[:index] + r + data[index + len(filename):]
    return data


def findCodeblocks(data: str) -> [(int, int)]:
    backticks = '```'
    ret = []

    startIndex = 0

    while (index := data.find(backticks, startIndex)) >= 0:
        if index == -1:
            return ret

        try:
            closingIndex = data.find(backticks, index + len(backticks))
            if closingIndex < 0:
                return ret
            closingIndex += len(backticks)
            ret.append((index, closingIndex))
            startIndex = closingIndex
        except:
            return ret

    return ret


def splitData(data: str, codeblocks: [{int, int}]) -> [(str, bool)]:
    result = []
    index = 0
    for start, end in codeblocks:
        result.append((data[index:start], False))
        result.append((data[start:end], True))
        index = end

    result.append((data[index:], False))
    return result


def replaceData(data, replacements):
    blocks = splitData(data, findCodeblocks(data))
    result = ''

    for block, isCode in blocks:
        if isCode:
            result += block
            continue

        for k, r in replacements.items():
            def fn(matchObj):
                g = matchObj.groups()
                return r.format(*g)

            try:
                block = re.sub(k, fn, block)
            except:
                pass

        result += block

    return result


def createHtml(markdown):
    html = common.convertMarkdownToHtml(markdown)
    return common.handleImages(html)


def processMarkdown(markdown, baseDir, replacements):
    markdown = referenceNotes(markdown, baseDir)
    return replaceData(markdown, replacements)


class TopBar(QWidget):
    nextPressed = pyqtSignal()
    prevPressed = pyqtSignal()

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        layout = QHBoxLayout()
        btnPrev = QPushButton('prev')
        btnNext = QPushButton('next')
        self.leSearch = QLineEdit()

        btnPrev.clicked.connect(self.prevPressed)
        btnNext.clicked.connect(self.nextPressed)
        self.leSearch.editingFinished.connect(self.nextPressed)

        layout.addWidget(self.leSearch)
        layout.addWidget(btnPrev)
        layout.addWidget(btnNext)
        layout.setContentsMargins(0, 0, 2, 0)

        self.setLayout(layout)

    def searchText(self):
        return self.leSearch.text()

    def setSearchText(self, text):
        self.leSearch.setText(text)

    def focusLineEdit(self):
        self.leSearch.setFocus()
        self.leSearch.selectAll()


class NoteWidget(QWidget):
    filenameChanged = pyqtSignal(str, str)
    linkClicked = pyqtSignal(str)
    htmlViewActivated = pyqtSignal()

    def __init__(self, settings, data_model, parent=None):
        QWidget.__init__(self, parent)
        self.settings = settings

        self.data_model = data_model
        self.data_model.noteRenamed.connect(self.noteRenamed)

        self.dir = None
        self.filename = None
        self.topBar = TopBar(self)
        self.topBar.prevPressed.connect(self.jumpToPrevResult)
        self.topBar.nextPressed.connect(self.jumpToNextResult)
        self.topBar.hide()

        self.markdownEditor = MarkdownTextEditor(self.settings, self)
        self.markdownEditor.zoomFactorChanged.connect(self.markdownZoomFactorChanged)
        self.markdownEditor.saveTriggered.connect(self.save)
        self.markdownEditor.setZoomFactor(self.settings.loadZoomFactor())

        # self.presenter = DocumentPresenter(self)
        self.presenter = WebEnginePresenter(self.settings, self)

        self.stackedWidget = QStackedWidget()
        self.stackedWidget.addWidget(self.markdownEditor)
        self.stackedWidget.addWidget(self.presenter.widget())
        self.stackedWidget.setCurrentIndex(1)

        def onSearchTriggered():
            self.topBar.show()
            self.topBar.focusLineEdit()

        self.triggerSearchShortcut = QShortcut(QKeySequence('Ctrl+Shift+f'), self, onSearchTriggered)
        self.hideSearchShortcut = QShortcut(QKeySequence('Esc'), self.topBar, self.topBar.hide)

        layout = QVBoxLayout()
        layout.addWidget(self.topBar)
        layout.addWidget(self.stackedWidget)
        layout.setContentsMargins(0, 0, 2, 0)

        self.setLayout(layout)

    def setSearchTerm(self, text):
        self.topBar.setSearchText(text)

    def jump(self, text, backward):
        if self.presenter.isVisible():
            return self.presenter.findText(text, backward)
        else:
            return self.markdownEditor.findText(text, backward)

    def jumpToPrevResult(self):
        self.jump(self.topBar.searchText(), backward=True)

    def jumpToNextResult(self):
        self.jump(self.topBar.searchText(), backward=False)

    def onLinkClicked(self, url):
        self.linkClicked.emit(url)

    def showMarkdown(self):
        self.stackedWidget.setCurrentIndex(0)
        self.markdownEditor.setFocus()
        self.markdownEditor.jumpToEnd()

    def showHtml(self):
        markdown = processMarkdown(self.markdownEditor.toPlainText(), self.dir, self.settings.loadReplacements())
        html = createHtml(markdown)
        with open('/tmp/bla.html', 'w') as f:
            f.write(html)
        self.presenter.setHtml(html)
        self.stackedWidget.setCurrentIndex(1)
        self.htmlViewActivated.emit()

    def setMarkdown(self, filename, directory):
        path = os.path.join(directory, filename)
        with open(path) as f:
            markdown = str(f.read())

        self.filename = filename
        self.dir = directory
        self.markdownEditor.setPlainText(markdown)

        if self.hasFocus():
            self.showMarkdown()
        else:
            self.showHtml()

    def save(self):
        markdown = self.markdownEditor.toPlainText()
        self.data_model.saveNote(self.filename, markdown)
        self.showHtml()

    def noteRenamed(self, oldName, newName):
        if self.filename == oldName:
            self.filename = newName

    def markdownZoomFactorChanged(self, zoomFactor: float):
        self.presenter.setZoom(zoomFactor)
        self.settings.saveZoomFactor(zoomFactor)

    def webViewZoomFactorChanged(self, zoomFactor: float):
        self.markdownEditor.setZoomFactor(zoomFactor)
        self.settings.saveZoomFactor(zoomFactor)
