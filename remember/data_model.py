from PyQt6.QtCore import QObject, pyqtSignal

import common


class DataModel(QObject):
    filesChanged = pyqtSignal()
    noteRenamed = pyqtSignal(str, str)
    noteAdded = pyqtSignal(str)
    noteRemoved = pyqtSignal(str)
    currentDirChanged = pyqtSignal(str)

    def __init__(self, settings, parent=None):
        QObject.__init__(self, parent)

        self._settings = settings
        self._baseDir = self._settings.loadDirectory()
        self._files = common.getFiles(self._baseDir)

    def baseDir(self):
        return self._baseDir

    def files(self):
        return self._files

    def count(self):
        return len(self._files)

    def create(self):
        title = common.createNewNote(self.baseDir())
        filename = common.getFilenameFromTitle(title)
        self._files.append(filename)
        self.noteAdded.emit(filename)

    def remove(self, filename):
        try:
            common.deleteNote(self._baseDir, filename)
            self._files.remove(filename)
            self.noteRemoved.emit(filename)
        except:
            pass

    def rename(self, oldFilename, newFilename):
        self._files.remove(oldFilename)
        self._files.append(newFilename)
        self._files.sort()
        self.noteRenamed.emit(oldFilename, newFilename)

    def saveNote(self, filename, markdown):
        newTitle = common.getTitleFromContent(markdown)
        newFilename = common.getFilenameFromTitle(newTitle)

        if filename != newFilename:
            newFilename = common.renameNote(self.baseDir(), filename, markdown)
            self.rename(filename, newFilename)
        else:
            common.saveNote(self.baseDir(), filename, markdown)

    def setBaseDir(self, baseDir):
        self._settings.saveDirectory(baseDir)
        self._baseDir = baseDir
        self._files = common.getFiles(self._baseDir)
        self.currentDirChanged.emit(self._baseDir)

    def createBaseDir(self, baseDir):
        dirs = self.allDirectories()
        dirs.append(baseDir)
        self._settings.saveLastDirectories(dirs)

    def removeBaseDir(self, baseDir):
        dirs = self.allDirectories()
        try:
            dirs.remove(baseDir)
            self._settings.saveLastDirectories(dirs)
            dirs = self.allDirectories()
            if len(dirs):
                self.setBaseDir(dirs[0])

        except:
            pass

    def allDirectories(self):
        dirs = self._settings.loadLastDirectories()
        return dirs if dirs else []

    def setFilter(self, filter):
        def isFilterValid(filter):
            return filter and len(filter) > 0

        allFiles = common.getFiles(self.baseDir())
        self._files = common.filterTitles(self.baseDir(), allFiles, filter) \
            if isFilterValid(filter) \
            else allFiles
        self.filesChanged.emit()
