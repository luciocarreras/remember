import pytest

from data_model import DataModel
from history import History
from main_window import MainWindow
from settings import Settings

from PyQt6.QtCore import QSettings

import os
import shutil
import sys


@pytest.fixture
def notes_directory(tmp_path):
    tmp = tmp_path / "remember-test"
    tmp.mkdir()

    yield tmp

    shutil.rmtree(tmp)


def write_note(directory, index):
    filename = "note {}.md".format(index)
    filepath = directory / filename
    is_nice = 'nice' if ((index % 2) == 0) else 'ugly'
    with open(filepath, 'w') as f:
        text = f'''# note {index}
        Text {index}
        {is_nice}'''
        f.write(text)

    return filename


def organization():
    return "RememberTest"


def appName():
    return "remember-test"


@pytest.fixture
def note_count(request):
    return getattr(request, 'param', 10)


@pytest.fixture
def notes(note_count, notes_directory):
    filenames = []
    for i in range(0, note_count - 1, 1):
        filename = write_note(notes_directory, i)
        filenames.append(filename)

    yield filenames


@pytest.fixture
def data_model(notes, settings):
    return DataModel(settings)


@pytest.fixture
def history(data_model):
    return History(data_model)


# https://github.com/pytest-dev/pytest-qt/issues/483
@pytest.fixture(scope="session")
def qapp_args():
    return [sys.argv[0]]


@pytest.fixture
def window(settings, data_model, history):
    window = MainWindow(settings, data_model, history)
    window.show()
    return window


@pytest.fixture
def replacement_file(request):
    f = QSettings(organization(), appName()).fileName()
    d = os.path.dirname(f)
    if not os.path.exists(d):
        os.makedirs(d, exist_ok=True)
    path = os.path.join(d, 'replacements')
    with open(path, 'w') as file:
        file.write(getattr(request, 'param', ''))
    yield
    os.remove(path)


@pytest.fixture
def settings_content(request):
    if attr := getattr(request, 'param', None):
        file_content = attr
        qs = QSettings(organization(), appName())
        if not os.path.exists(os.path.dirname(qs.fileName())):
            os.mkdir(os.path.dirname(qs.fileName()))
        for k, v in file_content.items():
            qs.setValue(k, v)


@pytest.fixture
def settings(replacement_file, notes_directory, settings_content):
    s = Settings(organization(), appName())
    s.saveDirectory(str(notes_directory.absolute()))

    yield s

    qs = QSettings(organization(), appName())
    os.remove(qs.fileName())
