from presenter import Presenter

from PyQt6.QtWidgets import QTextBrowser
from PyQt6.QtGui import QTextDocument


class DocumentPresenter(Presenter):
    def __init__(self, parent):
        Presenter.__init__(self)
        self.textBrowser = QTextBrowser(parent)
        self.textBrowser.setOpenLinks(True)
        self.textBrowser.setOpenExternalLinks(True)

    def css(self):
        return '''    
h3
{
    border: 1px solid white;
    font-size: x-large;
}

h4
{
    font-size: large;
}

h5
{
    font-size: medium;
}

a
{
    color: #46c;
}

.cp
{
    color: #7c7;
}

.cpf, .n, .nf
{
    color: #cc4;
}

.mi
{
    color: #9c9;
}

.s
{
    color: #88e;
}

.codehilite
{
    border: 1px solid #aaa;
    margin: 1ex;
    padding: 10px;
}

code, pre
{
    font-family: "Courier";
    font-size: medium;
    padding: 10px;
}

pre
{
    background: #111;
    color: #9c9;
    line-height: 1.5em;
    padding-left: 1em;
}
'''

    def setHtml(self, html):
        doc = QTextDocument(self)
        css = self.css()
        print(css)
        doc.setDefaultStyleSheet(css)
        self.textBrowser.setStyleSheet(css)
        print(html)
        doc.setHtml(html)
        self.textBrowser.setDocument(doc)

    def findText(self, text, backward):
        flag = QTextDocument.FindFlag.FindBackward if backward else QTextDocument.FindFlag(0)
        self.textBrowser.find(text, flag)

    def setZoom(self, zoom: float):
        pass

    def widget(self):
        return self.textBrowser

    def isVisible(self) -> bool:
        return self.textBrowser.isVisible()
