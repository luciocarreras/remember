#!/usr/bin/env python3

import sys

from main_window import MainWindow
from settings import Settings
from data_model import DataModel
from history import History

from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QApplication


def main():
    QApplication.setAttribute(Qt.ApplicationAttribute.AA_ShareOpenGLContexts, False)
    app = QApplication(sys.argv)
    settings = Settings("Remember", "remember")
    dataModel = DataModel(settings)
    history = History(dataModel)
    mainWindow = MainWindow(settings, dataModel, history)
    mainWindow.show()
    sys.exit(app.exec())


if __name__ == '__main__':
    main()
