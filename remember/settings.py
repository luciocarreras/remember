from PyQt6.QtCore import (Qt, QSettings)
import os


class Settings:
    def __init__(self, organization, application):
        self.settings = QSettings(organization, application)

        d = os.path.dirname(self.settings.fileName())
        self.replacementFile = os.path.join(d, 'replacements')

    def loadDirectory(self):
        return self.settings.value("directory")

    def saveDirectory(self, directory):
        self.settings.setValue("directory", directory)

    def saveLastNote(self, filename):
        self.settings.setValue("last_note", filename)

    def loadLastNote(self):
        return self.settings.value("last_note")

    def loadLastDirectories(self):
        return self.settings.value("last_directories")

    def saveLastDirectories(self, directories):
        self.settings.setValue("last_directories", directories)

    def loadSplitterState(self, ):
        return self.settings.value("ui/splitter")

    def saveSplitterState(self, splitterState):
        self.settings.setValue("ui/splitter", splitterState)

    def loadGeometry(self, ):
        return self.settings.value("ui/size")

    def saveGeometry(self, geometry):
        self.settings.setValue("ui/size", geometry)

    def loadZoomFactor(self):
        val = self.settings.value("ui/zoom", 1.0)
        return float(val)

    def saveZoomFactor(self, zoomFactor):
        self.settings.setValue("ui/zoom", zoomFactor)

    def loadReplacements(self):
        replacements = dict()

        try:
            with open(self.replacementFile) as f:
                lines = f.readlines()

            for line in lines:
                line = line.strip('\n')
                items = line.split(': ', 1)
                replacements[items[0]] = items[1]
        finally:
            return replacements

    def saveReplacements(self, replacements):
        lines = [k + ': ' + v + '\n' for k, v in replacements.items()]
        with open(self.replacementFile, 'w') as f:
            f.writelines(lines)
