import common
import pytest
from note_widget import NoteWidget, processMarkdown, createHtml, findCodeblocks, splitData

from PyQt6.QtCore import Qt, QUrl
from PyQt6.QtWebEngineCore import QWebEnginePage
from PyQt6.QtGui import QImage

import re
import os


@pytest.fixture
def note_widget(settings, data_model):
    noteWidget = NoteWidget(settings, data_model)
    yield noteWidget


def find_file(notes_directory, name):
    files = os.listdir(notes_directory)
    return files.count(name)


def find_in_list_widget(list_widget, name):
    items = list_widget.findItems(name, Qt.MatchFlag.MatchExactly)
    return len(items)


def get_markdown_text(markdown_editor):
    return markdown_editor.toPlainText()


def validate_file_change_on_save(note_widget, old_filename, expected_filename, markdown, directory):
    def onFilenameChanged(_, new_filename):
        assert new_filename == expected_filename

    note_widget.filenameChanged.connect(onFilenameChanged)
    note_widget.setMarkdown(old_filename, directory)

    assert find_file(directory, old_filename) == 1
    assert find_file(directory, expected_filename) == 0
    assert get_markdown_text(note_widget.markdownEditor) != markdown
    assert note_widget.filename == old_filename

    note_widget.markdownEditor.setPlainText(markdown)
    note_widget.save()

    assert find_file(directory, old_filename) == 0
    assert find_file(directory, expected_filename) == 1
    assert get_markdown_text(note_widget.markdownEditor) == markdown
    assert note_widget.filename == expected_filename

    filepath = os.path.join(directory, expected_filename)
    with open(filepath) as f:
        data = str(f.read())
        assert data == markdown


def test_filename_stays_same_at_save(note_widget, data_model, notes_directory, qtbot):
    def assertFalse(old_name, new_name):
        assert False

    data_model.noteRenamed.connect(assertFalse)

    markdown = """# note 1
        ST:TOS
    """

    old_filename = 'note 1.md'
    note_widget.setMarkdown(old_filename, notes_directory)
    note_widget.markdownEditor.setPlainText(markdown)
    note_widget.save()

    assert get_markdown_text(note_widget.markdownEditor) == markdown
    assert note_widget.filename == old_filename
    assert find_file(notes_directory, old_filename) == 1


def test_filename_changes_on_save(note_widget, notes_directory, qtbot):
    markdown = """# Some Title
        And just for fun
    """
    validate_file_change_on_save(note_widget, 'note 1.md', 'Some Title.md', markdown, notes_directory)


def test_filename_changes_on_conflicting_save(note_widget, notes_directory, qtbot):
    markdown = """# note 4
        And just for fun
    """
    validate_file_change_on_save(note_widget, 'note 1.md', 'note 41.md', markdown, notes_directory)


def test_filename_changes_on_empty_save(note_widget, notes_directory, qtbot):
    markdown = ''
    validate_file_change_on_save(note_widget, 'note 1.md', 'New.md', markdown, notes_directory)


def validate_link(noteWidget, title, note, referenced_note, directory):
    # noinspection PyStringFormat
    markdown = f'''# {title}

Referencing {referenced_note} here

    '''.format(title, referenced_note)

    common.saveNote(directory, note, markdown)
    noteWidget.setMarkdown(note, directory)

    markdown = processMarkdown(noteWidget.markdownEditor.toPlainText(), directory, {})
    html = createHtml(markdown)

    encoded_note = str(QUrl.toPercentEncoding(referenced_note), 'utf-8')
    link = f'<a href="remember:///{encoded_note}">{referenced_note}</a>'

    return link.lower() in html


def test_create_link(note_widget, notes, notes_directory, qtbot):
    assert validate_link(note_widget, 'note', notes[0], notes[1], notes_directory)


def test_create_link_case_insensitive(note_widget, notes, notes_directory, qtbot):
    assert validate_link(note_widget, 'note', notes[0], notes[1].upper(), notes_directory)


def test_dont_create_link_for_non_existing_file(note_widget, notes, notes_directory, qtbot):
    assert validate_link(note_widget, 'note', notes[0], 'note27.md', notes_directory) is False


def test_note_widget_with_window_save(window, data_model, notes_directory, qtbot):
    old_filename = 'note 5.md'
    expected_filename = 'Some Title.md'
    window.listWidget.setCurrentRow(5)
    assert window.listWidget.currentItem().text() == old_filename

    noteWidget = window.noteWidget

    markdown = """# Some Title
        And just for fun
    """

    assert data_model.files().count(old_filename) == 1
    assert data_model.files().count(expected_filename) == 0
    assert find_in_list_widget(window.listWidget, old_filename) == 1
    assert find_in_list_widget(window.listWidget, expected_filename) == 0

    validate_file_change_on_save(noteWidget, old_filename, expected_filename, markdown, notes_directory)

    assert data_model.files().count(old_filename) == 0
    assert data_model.files().count(expected_filename) == 1
    assert find_in_list_widget(window.listWidget, old_filename) == 0
    assert find_in_list_widget(window.listWidget, expected_filename) == 1


def test_list_view_has_focus_after_save(window, data_model, notes, qtbot):
    noteWidget = window.noteWidget

    window.listWidget.setCurrentRow(5)
    filename = window.listWidget.currentItem().text()
    noteWidget.setMarkdown(filename, data_model.baseDir())
    noteWidget.showMarkdown()

    qtbot.waitUntil(lambda: noteWidget.markdownEditor.hasFocus(), timeout=1000)
    assert noteWidget.markdownEditor.hasFocus()

    noteWidget.save()

    qtbot.waitUntil(lambda: window.listWidget.hasFocus(), timeout=1000)
    assert window.listWidget.hasFocus()


@pytest.mark.parametrize('note_name', ['note 3.md', 'NOTE 3.MD'])
def test_click_link(window, note_name, qtbot):
    window.listWidget.setCurrentRow(5)
    try:
        page = window.noteWidget.presenter.webEngineView.page()
        page.acceptNavigationRequest(QUrl(f'remember:///{note_name}'),
                                     QWebEnginePage.NavigationType.NavigationTypeLinkClicked, True)
        assert window.listWidget.currentRow() == 3
    except:
        pass


def create_editor(window):
    window.listWidget.setCurrentRow(5)
    return window, window.noteWidget.markdownEditor


def test_paste_image(window, notes_directory, qapp):
    _, editor = create_editor(window)

    image = QImage(3, 3, QImage.Format.Format_RGB32)
    qapp.clipboard().setImage(image)

    editor.insertFromMimeData(qapp.clipboard().mimeData())
    re_string = r'!\[\]\({}/images/clipboard-.*?\.png\)'.format(notes_directory)
    regexp = re.compile(re_string)
    assert regexp.search(editor.toPlainText())


def test_paste_text(window, qapp):
    text = 'some text'
    _, editor = create_editor(window)
    editor.setPlainText('')

    qapp.clipboard().setText(text)
    editor.insertFromMimeData(qapp.clipboard().mimeData())
    assert editor.toPlainText() == text


def test_find_codeblocks():
    data = '''# header
text which needs to be replaced

```
int main()
do not replace anything here
``` 

replace this

```
this is code, do not replace it
``` but this should be replaced
'''
    codeBlocks = findCodeblocks(data)

    assert len(codeBlocks) == 2

    start, end = codeBlocks[0]
    assert data[start:end] == '''```
int main()
do not replace anything here
```'''

    start, end = codeBlocks[1]
    assert data[start:end] == '''```
this is code, do not replace it
```'''


def test_find_codeblocks_with_empty_str():
    data = ''
    codeBlocks = findCodeblocks(data)
    assert len(codeBlocks) == 0


def test_split_data():
    data = '''# header
text which needs to be replaced

```
int main()
do not replace anything here
``` 

replace this

```
this is code, do not replace it
``` but this should be replaced
'''

    codeblocks = findCodeblocks(data)
    splitted = splitData(data, codeblocks)

    assert splitted[0] == ('''# header
text which needs to be replaced

''', False)

    assert splitted[1] == ('''```
int main()
do not replace anything here
```''', True)

    assert splitted[2] == (''' 

replace this

''', False)

    assert splitted[3] == ('''```
this is code, do not replace it
```''', True)

    assert splitted[4] == (''' but this should be replaced
''', False)
