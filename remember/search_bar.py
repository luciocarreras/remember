import common
import os
import settings

from PyQt6.QtWidgets import (QWidget, QLineEdit, QPushButton, QSizePolicy, QHBoxLayout)
from PyQt6.QtGui import QKeySequence, QShortcut
from PyQt6.QtCore import Qt, QEvent, pyqtSlot, pyqtSignal

class SearchBar(QWidget):
    textChanged = pyqtSignal(str)
    newClicked = pyqtSignal(bool)
    deleteClicked = pyqtSignal(bool)
    editClicked = pyqtSignal(bool)

    def __init__(self, settings, parent=None):
        QWidget.__init__(self, parent)
        layout = QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)

        self.settings = settings
        self.btnNew = QPushButton(self)
        self.btnNew.setIcon(common.getAddIcon())
        self.btnDelete = QPushButton(self)
        self.btnDelete.setIcon(common.getRemoveIcon())
        self.btnEdit = QPushButton(self)
        self.btnEdit.setIcon(common.getEditIcon())
        self.searchField = QLineEdit(self)
        self.searchField.setPlaceholderText("Search")
        self.searchField.setSizePolicy(
                QSizePolicy(QSizePolicy.Policy.MinimumExpanding, QSizePolicy.Policy.Preferred))
        layout.addWidget(self.btnNew)
        layout.addWidget(self.btnDelete)
        layout.addWidget(self.btnEdit)
        layout.addWidget(self.searchField)

        self.setLayout(layout)

        self.btnNew.clicked.connect(self.newClicked)
        self.btnDelete.clicked.connect(self.deleteClicked)
        self.btnEdit.clicked.connect(self.editClicked)
        self.searchField.textEdited.connect(self.textChanged)
        self.shortcut = QShortcut(QKeySequence('Ctrl+f'), self.searchField, self.searchField.setFocus) 

    def setDeleteEnabled(self, b):
        self.btnDelete.setEnabled(b)

