import common

from note_widget import NoteWidget
from search_bar import SearchBar
from bottom_bar import BottomBar

from PyQt6.QtWidgets import (QWidget, QListWidget, QMessageBox, QSplitter, QHBoxLayout, QVBoxLayout)
from PyQt6.QtGui import QKeySequence, QShortcut, QDesktopServices
from PyQt6.QtCore import Qt, QUrl, QObject, pyqtSlot


def showDeleteDialog(text, parent):
    return QMessageBox(QMessageBox.Icon.Question, 'Delete', text,
                       QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No, parent)


def selectByText(listWidget, text):
    if not text or len(text) == 0:
        return

    for i in range(0, listWidget.count()):
        item = listWidget.item(i)
        if item.text().lower() == text.lower():
            listWidget.setCurrentItem(item)
            return


class Caller(QObject):
    def __init__(self, listWidget):
        QObject.__init__(self, listWidget)
        self.listWidget = listWidget

    @pyqtSlot(QUrl)
    def handler(self, url: QUrl):
        selectByText(self.listWidget, url.host())


class MainWindow(QWidget):
    def __init__(self, settings, model, history, parent=None):
        QWidget.__init__(self, parent)

        self.model = model
        self.model.filesChanged.connect(self.populateListWidget)
        self.model.currentDirChanged.connect(self.baseDirChanged)
        self.model.noteRenamed.connect(self.noteRenamed)
        self.model.noteAdded.connect(self.noteAdded)
        self.model.noteRemoved.connect(self.noteRemoved)

        self.history = history
        self.settings = settings
        self.listWidget = QListWidget()
        self.listWidget.setAlternatingRowColors(True)
        self.noteWidget = NoteWidget(self.settings, self.model)

        leftWidget = QWidget(self)
        self.searchBar = SearchBar(settings)
        self.searchBar.setDeleteEnabled(model.count() > 0)
        self.bottomBar = BottomBar(settings)
        self.baseDirChanged(self.model.baseDir())

        leftLayout = QVBoxLayout(leftWidget)
        leftLayout.addWidget(self.searchBar)
        leftLayout.addWidget(self.listWidget)
        leftLayout.addWidget(self.bottomBar)
        leftLayout.setContentsMargins(0, 0, 2, 0)
        leftWidget.setLayout(leftLayout)

        layoutMain = QHBoxLayout(self)
        self.splitterMain = QSplitter(self)
        self.splitterMain.addWidget(leftWidget)
        self.splitterMain.addWidget(self.noteWidget)

        baseDir = self.model.baseDir()
        title = baseDir if baseDir else "Notes"
        self.setWindowTitle(title)

        layoutMain.addWidget(self.splitterMain)
        self.setLayout(layoutMain)
        self.populateListWidget()

        self.noteWidget.linkClicked.connect(self.onLinkClicked)
        self.noteWidget.htmlViewActivated.connect(self.listWidget.setFocus)
        self.listWidget.currentItemChanged.connect(self.currentListItemChanged)
        self.searchBar.textChanged.connect(self.searchTextEdited)
        self.searchBar.newClicked.connect(self.newClicked)
        self.searchBar.deleteClicked.connect(self.deleteClicked)
        self.searchBar.editClicked.connect(self.focusEditor)

        self.bottomBar.addDirClicked.connect(self.newDirClicked)
        self.bottomBar.deleteDirClicked.connect(self.deleteDirClicked)
        self.bottomBar.dirSelected.connect(self.model.setBaseDir)

        self.editShortcut = QShortcut(QKeySequence('Ctrl+e'), self.noteWidget, self.focusEditor)
        self.listShortcut = QShortcut(QKeySequence('Ctrl+l'), self.listWidget, self.listWidget.setFocus)
        self.newShortcut = QShortcut(QKeySequence('Ctrl+n'), self, self.newClicked)
        self.deleteShortcut = QShortcut(QKeySequence('Ctrl+d'), self, self.deleteClicked)
        self.nextShortcut = QShortcut(QKeySequence(Qt.Modifier.ALT | Qt.Key.Key_Right), self, self.historyForward)
        self.prevShortcut = QShortcut(QKeySequence(Qt.Modifier.ALT | Qt.Key.Key_Left), self, self.historyBackward)

        self.deleteDialog = None

        selectByText(self.listWidget, self.settings.loadLastNote())
        if not self.listWidget.currentItem() and self.listWidget.count() > 0:
            self.listWidget.setCurrentRow(0)

        self.initUrlHandler()

    def __del__(self):
        for urlHandler in ['http', 'https', 'file', 'ftp', 'remember']:
            QDesktopServices.unsetUrlHandler(urlHandler)

    @pyqtSlot(QUrl)
    def urlHandler(self, url: QUrl):
        if url.scheme() == "remember":
            selectByText(self.listWidget, url.host())
        else:
            QDesktopServices.openUrl(url)

    def initUrlHandler(self):
        for f in ['http', 'https', 'file', 'ftp', 'remember']:
            QDesktopServices.unsetUrlHandler(f)
            QDesktopServices.setUrlHandler(f, self.urlHandler)

    def populateListWidget(self):
        currentItem = self.listWidget.currentItem()
        currentText = currentItem.text() if currentItem else ''

        self.listWidget.clear()
        for filename in self.model.files():
            self.listWidget.addItem(filename)
            if currentText == filename:
                self.listWidget.setCurrentRow(self.listWidget.count() - 1)

        self.listWidget.sortItems()
        self.listWidget.setEnabled(self.model.count() > 0)
        self.searchBar.setDeleteEnabled(self.model.count() > 0)

    def currentListItemChanged(self, current, previous):
        if current:
            filename = current.text()
            self.history.changeItem(filename)
            self.settings.saveLastNote(filename)
            self.noteWidget.setMarkdown(filename, self.model.baseDir())

    def listWidgetItem(self, item):
        items = self.listWidget.findItems(item, Qt.MatchFlag.MatchFixedString)
        if len(items) > 0:
            return items[0]
        return None

    def historyJump(self, forward: bool):
        note = self.history.nextItem() if forward else self.history.previousItem()
        if widgetItem := self.listWidgetItem(note):
            self.history.forward() if forward else self.history.backward()
            self.listWidget.setCurrentItem(widgetItem)

    def historyForward(self):
        self.historyJump(True)

    def historyBackward(self):
        self.historyJump(False)

    def focusEditor(self):
        self.noteWidget.showMarkdown()

    def newClicked(self, b=False):
        if (baseDir := self.model.baseDir()) is None:
            self.newDirClicked()
        else:
            self.model.create()

    def noteAdded(self, filename):
        self.populateListWidget()
        selectByText(self.listWidget, filename)
        self.noteWidget.showMarkdown()

    def noteRenamed(self, oldFilename, newFilename):
        self.populateListWidget()
        selectByText(self.listWidget, newFilename)

    def deleteClicked(self, b=False):
        currentItem = self.listWidget.currentItem()
        if currentItem:
            currentNote = currentItem.text()
            self.deleteDialog = showDeleteDialog(f'Do you really want to delete Note "{currentNote}"?', self)
            if self.deleteDialog.exec() == QMessageBox.StandardButton.Yes:
                self.model.remove(currentNote)

    def noteRemoved(self, filename):
        currentRow = self.listWidget.currentRow()
        self.populateListWidget()
        if 0 <= currentRow < self.listWidget.count():
            self.listWidget.setCurrentRow(currentRow)
        elif self.listWidget.count() > 0:
            self.listWidget.setCurrentRow(self.listWidget.count() - 1)

    def baseDirChanged(self, directory):
        title = directory if directory else "Notes"
        self.setWindowTitle(title)
        self.bottomBar.setCurrentDir(directory)
        self.populateListWidget()

    def newDirClicked(self):
        directory = common.chooseNewDir(self)
        if len(directory):
            self.model.createBaseDir(directory)
            self.model.setBaseDir(directory)

    def deleteDirClicked(self, directory):
        self.deleteDialog = showDeleteDialog(
            f'''Do you really want to remove "{directory}"?
    Files and directory won\'t be deleted.''', self)
        if self.deleteDialog.exec() == QMessageBox.StandardButton.Yes:
            self.model.removeBaseDir(directory)

    def searchTextEdited(self, text):
        self.noteWidget.setSearchTerm(text)
        self.model.setFilter(text)
        self.searchBar.searchField.setFocus()

    def onLinkClicked(self, filename):
        selectByText(self.listWidget, filename)

    def showEvent(self, event):
        if geometry := self.settings.loadGeometry():
            self.restoreGeometry(geometry)
        else:
            self.setGeometry(50, 50, 800, 600)

        if splitterState := self.settings.loadSplitterState():
            self.splitterMain.restoreState(splitterState)

    def closeEvent(self, event):
        self.settings.saveSplitterState(self.splitterMain.saveState())
        self.settings.saveGeometry(self.saveGeometry())
        super(MainWindow, self).closeEvent(event)
