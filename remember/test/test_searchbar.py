from search_bar import SearchBar


def test_search_text(settings, qtbot):
    searchBar = SearchBar(settings)
    searchBar.show()
    searchBar.setFocus()
    qtbot.addWidget(searchBar)

    def onSearchTextChanged(text):
        global callback_called
        callback_called = True

        assert len(text) > 0 and 'nice'.startswith(text)

    searchBar.textChanged.connect(onSearchTextChanged)
    qtbot.keyClicks(searchBar.searchField, 'nice')

    assert callback_called == True


def test_search_text_with_window(window, notes, qtbot):
    searchBar = window.searchBar
    assert window.listWidget.count() == len(notes)

    searchBar.searchField.setFocus()
    qtbot.waitUntil(lambda: searchBar.searchField.hasFocus(), timeout=1000)
    assert searchBar.searchField.hasFocus()

    qtbot.keyClicks(searchBar.searchField, 'nice')

    assert window.listWidget.count() < len(notes)

    qtbot.waitUntil(lambda: searchBar.searchField.hasFocus(), timeout=1000)
    assert searchBar.searchField.hasFocus()
