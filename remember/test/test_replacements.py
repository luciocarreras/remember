import pytest
from note_widget import processMarkdown


@pytest.mark.parametrize('replacement_file', ['''like: behave like
dogs: monkeys
cats: donkeys'''], indirect=True)
def test_standard_replacement(notes, notes_directory, settings, qtbot):
    # noinspection PyStringFormat
    markdown = f'''# title
        Some people like dogs and some people prefer cats'''

    replacements = settings.loadReplacements()
    new_markdown = processMarkdown(markdown, notes_directory, replacements)

    expected_markdown = '''# title
        Some people behave like monkeys and some people prefer donkeys'''
    assert new_markdown == expected_markdown


@pytest.mark.parametrize('replacement_file', ['(dogs|cats): white {0}'], indirect=True)
def test_regex_replacement(notes_directory, settings, qtbot):
    # noinspection PyStringFormat
    markdown = '''# title
        Some people like dogs and some people prefer cats'''

    replacements = settings.loadReplacements()
    new_markdown = processMarkdown(markdown, notes_directory, replacements)

    expected_markdown = '''# title
        Some people like white dogs and some people prefer white cats'''
    assert new_markdown == expected_markdown


@pytest.mark.parametrize('replacement_file', ['''(dogs|cats): white {0}
white dogs: red pandas'''], indirect=True)
def test_consecutive_replacements(notes_directory, settings, qtbot):
    # noinspection PyStringFormat
    markdown = f'''# title
        Some people like dogs and some people prefer cats'''

    replacements = settings.loadReplacements()
    new_markdown = processMarkdown(markdown, notes_directory, replacements)

    expected_markdown = '''# title
        Some people like red pandas and some people prefer white cats'''
    assert new_markdown == expected_markdown


@pytest.mark.parametrize('replacement_file', ['(dogs|cats): white {1}'], indirect=True)
def test_invalid_replacement(notes_directory, settings, qtbot):
    # noinspection PyStringFormat
    markdown = '''# title
        Some people like dogs and some people prefer cats'''

    replacements = settings.loadReplacements()
    new_markdown = processMarkdown(markdown, notes_directory, replacements)
    assert new_markdown == markdown


@pytest.mark.parametrize('replacement_file', ['dogsadfasdmonkeys'], indirect=True)
def test_wrong_syntax_no_colon(settings, notes_directory):
    markdown = '''# title
        Some people like dogs and some people prefer cats'''
    replacements = settings.loadReplacements()
    new_markdown = processMarkdown(markdown, notes_directory, replacements)

    assert new_markdown == markdown


@pytest.mark.parametrize('replacement_file', ['text: replacement'], indirect=True)
def test_no_replacement_within_codeblocks(settings, notes_directory):
    markdown = '''# title
Some text
```
text inside code text
```
More text'''

    replacements = settings.loadReplacements()
    new_markdown = processMarkdown(markdown, notes_directory, replacements)
    expected_markdown = '''# title
Some replacement
```
text inside code text
```
More replacement'''
    assert new_markdown == expected_markdown

