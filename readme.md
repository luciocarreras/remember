# Needed python packages

* markdown
* pygments
* pyqt6
* PyQt6-WebEngine
* pyinstall
* pyinstaller

## Testing
* pytest
* pytest-qt
* pytest-xvfb

Alternatively, use
```
python3 -m pip install --upgrade pip
python3 -m pip install -r pypi/requirements.txt
```

# Ubuntu Focal
The following packages are necessary:
```
export DEBIAN_FRONTEND=noninteractive
apt install -y -q \
      sudo \
      python3-pip \
      libasound2 \
      libdbus-1-3 \
      libegl1\
      libfontconfig1 \
      libgl1-mesa-glx \
      libglib2.0-0 \
      libgssapi-krb5-2 \
      libnss3 \
      libxcomposite1 \
      libxdamage1 \
      libxkbcommon0 \
      libxkbfile1 \
      libxrandr2 \
      libxrender1 \
      libxtst6
```

## Build package
```
sudo ./install.sh
```

## Build and run in docker
```
docker build -t docker-remember -f Dockerfile
```
```
docker run \
  --rm \
  -it \
  --privileged \
  --network host \
  docker-remember /bin/bash
```

# Shortcuts

* Search: `Ctrl+f`
* Search in WebView widget: `Ctrl+Shift+f`
* New note: `Ctrl+n`
* Delete current note: `Ctrl+d`
* Edit: `Ctrl+e`
* Focus list: `Ctrl+l`

# Setting up replacements

Setup a file `$HOME/.config/Remember/replacements`. Inside this file you can specify regular expressions which should be replaced by longer and more complicated terms. e.g.

```
([^<\(])(http[s]?://\S+): {0}<{1}>
~(.*)~: <s>{0}</s>
(\s)(ISSUE-[0-9]+): {0}[{\1}](https://jira.company.com/browse/{\1})
g>(.*)<g: <p style="color: green;">{\0}</p>
```

Replace the `nth` match group with `{\n}` in the target expression.
