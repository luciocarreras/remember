import pytest
import os

import common

def fill_dir(num_files, prefix, directory):
    files = []
    for i in range(0, num_files, 1):
        if i == 0:
            filename = '{}.md'.format(prefix)
        else:
            filename = '{}{}.md'.format(prefix, i)

        filepath = os.path.join(directory, filename)
        with open(filepath, 'w') as f:
            f.write('File {}{}'.format(prefix, i))
        files.append(filename)

    return files


def check_file_content(directory, filename, content):
    path = os.path.join(directory, filename)
    with open(path) as f:
        data = str(f.read())
        return data == content
    return False

    
def create_note(title):
    return f'''# {title}
        Some data in it {title}
    '''.format(title);


def test_get_files_from_empty_dir(notes_directory):
    files = common.getFiles(notes_directory)
    assert len(files) == 0


def test_get_files(notes, notes_directory):
    for f in notes:
        assert f.startswith('note')
        assert f.endswith('.md')

    assert len(notes) == 9


def test_get_filename_from_title():
    assert common.getFilenameFromTitle('foo') == 'foo.md'
    assert common.getFilenameFromTitle('  ') == 'New.md'
    assert common.getFilenameFromTitle('foo*foo') == 'foo-foo.md'
    assert common.getFilenameFromTitle('foo/foo') == 'foo-foo.md'


def test_get_new_document_title(notes_directory):
    fill_dir(5, 'foo', notes_directory)
    title, path = common.getNewDocumentTitle(notes_directory, 'foo')
    assert title == 'foo5'
    assert path == os.path.join(notes_directory, 'foo5.md')

    title, path = common.getNewDocumentTitle(notes_directory, 'bar')
    assert title == 'bar'
    assert path == os.path.join(notes_directory, 'bar.md')
    
    fill_dir(2, 'bar', notes_directory)
    title, path = common.getNewDocumentTitle(notes_directory, 'bar')
    assert title == 'bar2'
    assert path == os.path.join(notes_directory, 'bar2.md')

    fill_dir(3, 'New', notes_directory)
    title, path = common.getNewDocumentTitle(notes_directory, '')
    assert title == 'New3'
    assert path == os.path.join(notes_directory, 'New3.md')


def test_delete_note(notes_directory):
    files = fill_dir(5, 'foo', notes_directory)
    files = common.getFiles(notes_directory)
    num_files = len(files)

    assert files.count('foo1.md') == 1
    common.deleteNote(notes_directory, 'foo1.md')
    files = common.getFiles(notes_directory)
    assert files.count('foo1.md') == 0
    assert len(files) == num_files - 1


def test_delete_not_existing_note(notes_directory):
    files = fill_dir(5, 'foo', notes_directory)
    files = common.getFiles(notes_directory)
    num_files = len(files)

    common.deleteNote(notes_directory, 'not-existing-note.md')

    files = common.getFiles(notes_directory)
    assert len(files) == num_files


def test_rename_note(notes_directory):
    files = fill_dir(5, 'foo', notes_directory)
    old_filename = 'foo1.md'
    new_filename = 'bar.md'
    markdown = create_note('bar')
    assert files.count(old_filename) == 1
    assert files.count(new_filename) == 0

    filename = common.renameNote(notes_directory, old_filename, markdown) 
    assert filename == new_filename

    files = common.getFiles(notes_directory)
    assert files.count(old_filename) == 0
    assert files.count(new_filename) == 1
    assert check_file_content(notes_directory, filename, markdown)


def test_rename_conflicting_note(notes_directory):
    old_filename = 'foo1.md'
    existing_filename = 'foo2.md'
    expected_filename = 'foo21.md'
    markdown = create_note('foo2')

    files = fill_dir(5, 'foo', notes_directory)

    assert files.count(old_filename) == 1
    assert files.count(existing_filename) == 1

    filename = common.renameNote(notes_directory, old_filename, markdown)

    files = common.getFiles(notes_directory)
    assert filename == expected_filename
    assert files.count(old_filename) == 0
    assert files.count(existing_filename) == 1
    assert files.count(expected_filename) == 1
    assert check_file_content(notes_directory, filename, markdown)


def test_references_change_on_renaming_notes(notes, notes_directory):
    old_filename = 'note 4.md'
    new_filename = 'note XYZ.md'

    paths = [os.path.join(notes_directory, 'note 1.md'),
             os.path.join(notes_directory, 'note 3.md')]

    for path in paths:
        with open(path, 'a') as f:
            f.write(f"reference to {old_filename}")

    new_markdown = f'''# {new_filename}
        nothing'''

    common.renameNote(notes_directory, old_filename, new_markdown)

    for path in paths:
        with open(path) as f:
            assert new_filename in f.read()


def test_save_new_note(notes_directory):
    fill_dir(1, 'bar', notes_directory)
    filename = 'foo.md'
    filepath = os.path.join(notes_directory, filename)
    assert not os.path.exists(filepath)

    new_data = '''# foo
        modified data'''


    assert common.saveNote(notes_directory, filename, new_data)

    assert filename == 'foo.md'
    assert check_file_content(notes_directory, filename, new_data)


def test_update_existing_note(notes_directory):
    fill_dir(1, 'bar', notes_directory)
    filename = 'bar.md'
    filepath = os.path.join(notes_directory, filename)
    assert os.path.exists(filepath)

    new_data = '''# bar
        modified data'''

    assert not check_file_content(notes_directory, filename, new_data)
    assert common.saveNote(notes_directory, filename, new_data)
    assert check_file_content(notes_directory, filename, new_data)

