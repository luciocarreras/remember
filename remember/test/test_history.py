from history import History


def create_history(items, data_model):
    history = History(data_model)
    for item in items:
        history.changeItem(item)
    return history


def test_empty_history(data_model):
    history = create_history([], data_model)
    assert history.backward() is None
    assert history.forward() is None
    assert history.previousItem() is None
    assert history.nextItem() is None


def test_history_backward(data_model):
    entries = ['a', 'b', 'c', 'd', 'e']
    history = create_history(entries, data_model)

    for i in range(len(entries) - 2, -1, -1):
        assert history.previousItem() == entries[i]
        assert history.backward() == entries[i]

    assert history.backward() is None
    assert history.backward() is None


def test_history_forward(data_model):
    entries = ['a', 'b', 'c', 'd', 'e']
    history = create_history(entries, data_model)

    while history.backward():
        pass

    for i in range(1, len(entries), 1):
        assert history.nextItem() == entries[i]
        assert history.forward() == entries[i]

    assert history.forward() is None
    assert history.forward() is None


def test_add_same_item_has_no_effect(data_model):
    entries = ['a', 'b', 'c', 'd', 'e']
    history = create_history(entries, data_model)

    while history.backward() != 'b':
        pass

    history.changeItem('b')
    history.changeItem('b')

    assert history.previousItem() == 'a'
    assert history.nextItem() == 'c'
    assert history.backward() == 'a'
    assert history.forward() == 'b'
    assert history.forward() == 'c'


def test_add_item_in_the_middle_changes_history(data_model):
    entries = ['a', 'b', 'c', 'd', 'e']
    history = create_history(entries, data_model)

    while history.backward() != 'b':
        pass

    history.changeItem('x')
    history.changeItem('y')
    history.changeItem('z')

    assert history.backward() == 'y'
    assert history.backward() == 'x'
    assert history.backward() == 'b'
    assert history.backward() == 'a'
    assert history.forward() == 'b'
    assert history.forward() == 'x'
    assert history.forward() == 'y'
    assert history.forward() == 'z'


def test_rename(data_model):
    entries = ['a', 'b', 'c', 'b', 'e']
    history = create_history(entries, data_model)

    history.itemRenamed('b', 'x')
    assert history.backward() == 'x'
    assert history.backward() == 'c'
    assert history.backward() == 'x'
    assert history.backward() == 'a'


def test_delete(data_model):
    entries = ['a', 'b', 'c', 'b', 'e', 'f']
    history = create_history(entries, data_model)

    history.itemRemoved('b')  # current index 'f'

    assert history.backward() == 'e'  # current index 'e'

    history.itemRemoved('e')  # current index 'c'
    assert history.backward() == 'a'


# todo: wrong behavior, fix me
def test_delete_might_result_in_double_entry(data_model):
    entries = ['a', 'b', 'c', 'b', 'e', 'f']
    history = create_history(entries, data_model)

    history.itemRemoved('c')  # current index 'f'

    assert history.backward() == 'e'
    assert history.backward() == 'b'
    assert history.backward() == 'b'
    assert history.backward() == 'a'
